from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.db.models import Q
from django_summernote.admin import SummernoteModelAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser, Project, Comment, RevisionComment, UserText, RevisionUserText, Tag, TextMeta
from .lib.get_project_from_request import get_project_from_request
from .lib.get_archive_hostname import get_archive_hostname

# custom input filter
class InputFilter(admin.SimpleListFilter):
    template = 'admin/input_filter.html'
    def lookups(self, request, model_admin):
        # Dummy, required to show the filter.
        return ((),)

    def choices(self, changelist):
        # Grab only the "all" option.
        all_choice = next(super().choices(changelist))
        all_choice['query_parts'] = (
            (k, v)
            for k, v in changelist.get_filters_params().items()
            if k != self.parameter_name
        )
        yield all_choice

class CommentRevisionTextSearchFilter(InputFilter):
    parameter_name = 'comment_revision_text'
    title = 'Comment Revision Text'

    def queryset(self, request, queryset):
        if self.value() is not None:
            search_term = self.value()
            return queryset.filter(
                revisions__text__icontains=search_term
            )

class CommentRevisionTitleSearchFilter(InputFilter):
    parameter_name = 'comment_revision_title'
    title = 'Comment Revision Title'

    def queryset(self, request, queryset):
        if self.value() is not None:
            search_term = self.value()
            return queryset.filter(
                revisions__title__icontains=search_term
            )

class CommentURNSearchFilter(InputFilter):
    parameter_name = 'comment_urn'
    title = 'Comment URN'

    def queryset(self, request, queryset):
        if self.value() is not None:
            search_term = self.value()
            return queryset.filter(
                urn__icontains=search_term
            )

class CommentCommenterFilter(InputFilter):
    parameter_name = 'comment_commenter'
    title = 'Commenter'

    def queryset(self, request, queryset):
        if self.value() is not None:
            search_term = self.value()
            return queryset.filter(
                Q(commenters__first_name__icontains=search_term) | Q(commenters__last_name__icontains=search_term)
            )

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ['email', 'username', 'full_name']
    fieldsets = UserAdmin.fieldsets + (
            (None, {'fields': ('full_name', 'bio', 'tagline', 'picture')}),
    )

class RevisionAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at','updated_at',)

class ProjectModelAdmin(SummernoteModelAdmin):
    summernote_fields = '__all__'

class TagModelAdmin(SummernoteModelAdmin):
    summernote_fields = '__all__'


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Project, ProjectModelAdmin)
admin.site.register(Tag, TagModelAdmin)
admin.site.register(Comment)
admin.site.register(RevisionComment, RevisionAdmin)
admin.site.register(UserText)
admin.site.register(RevisionUserText, RevisionAdmin)
admin.site.register(TextMeta)

# dashboard admin site
class Dashboard(AdminSite):
    pass

# initiate dashboard admin panel and customize labels
dashboard = Dashboard(name='dashboard')
dashboard.site_header = 'Commentary Dashboard'
dashboard.site_title = 'Commentary Dashboard'
dashboard.index_title = ''

class CommentarySettings(Project):
    class Meta:
        proxy = True
        verbose_name = 'Commentary Settings'
        verbose_name_plural = 'Commentary Settings'

class CommentarySettingsAdmin(SummernoteModelAdmin):
    readonly_fields = ('hostname',)
    # disable add action
    def has_add_permission(self, request, obj=None):
        return False
    # customized query set to overwrite filter with hostname related permissions
    def get_queryset(self, request):
        qs = super(CommentarySettingsAdmin, self).get_queryset(request)
        return qs.filter(hostname=get_archive_hostname(request), admins__id=request.user.id)

class UserProfile(CustomUser):
    class Meta:
        proxy = True
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profile'

class UserProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('username',)
    exclude = ('password', 'is_superuser', 'is_staff', 'is_active', 'last_login', 'date_joined', 'groups', 'user_permissions', 'full_name' )
    # disable add action
    def has_add_permission(self, request, obj=None):
        return False
    # customized query set to overwrite filter with hostname related permissions
    def get_queryset(self, request):
        qs = super(UserProfileAdmin, self).get_queryset(request)
        return qs.filter(id=request.user.id)

# config revision of comment to be inline editable objects of comment model
class RevisionCommentInline(admin.StackedInline):
    model = RevisionComment
    # max_num=maximun number of editable objects inline, extra=additional object that doesn't exceed max_num
    extra = 0
    readonly_fields = ('created_at','updated_at',)


# customize Comment model in admin panel
class CommentAdmin(admin.ModelAdmin):
    # add revisions of comment as inline editable object
    inlines = [
        RevisionCommentInline,
    ]

    list_display = ('urn', 'get_title', 'get_text', 'get_commenters', 'get_tags', 'comment_citation_urn', 'privacy', 'featured', 'project')
    readonly_fields = ('comment_citation_urn',)
    list_filter = [CommentRevisionTextSearchFilter, CommentRevisionTitleSearchFilter, CommentURNSearchFilter, CommentCommenterFilter, 'privacy',]

    # for list display many to many field on commenters
    def get_commenters(self, obj):
        return ", ".join([user.username for user in obj.commenters.all()])
    get_commenters.short_description = 'Commenters'

    # for list display many to many field on tags
    def get_tags(self, obj):
        return ", ".join([tag.name for tag in obj.tags.all()])
    get_tags.short_description = 'Tags'

    # show trimmed text from latest revision
    def get_text(self, obj):
        last_revision = obj.revisions.first()
        if last_revision and last_revision.text:
            if len(last_revision.text) > 80:
                short_text = last_revision.text[:80] + '...'
                return short_text
            else:
                return last_revision.text
        else:
            return ""
    get_text.short_description = 'Text'

    # show trimmed title from latest revision
    def get_title(self, obj):
        last_revision = obj.revisions.first()
        if last_revision and last_revision.title:
            if len(last_revision.title) > 80:
                short_text = last_revision.title[:80] + '...'
                return short_text
            else:
                return last_revision.title
        else:
            return ""
    get_title.short_description = 'Title'

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        # only show commenters from this project
        if db_field.name == "commenters":
            project = get_project_from_request(request)
            kwargs["queryset"] = project.commenters.all()
        return super(CommentAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # only allow project to be the current hostname
        if db_field.name == "project":
            kwargs["queryset"] = Project.objects.filter(hostname=get_archive_hostname(request))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    # customized query set to overwrite filter with hostname related permissions
    def get_queryset(self, request):
        qs = super(CommentAdmin, self).get_queryset(request)
        project = get_project_from_request(request)
        return qs.filter(project_id=project.id)


# customize Tag model in admin panel
class TagAdmin(admin.ModelAdmin):

    # customized query set to overwrite filter with hostname related permissions
    def get_queryset(self, request):
        qs = super(TagAdmin, self).get_queryset(request)
        project = get_project_from_request(request)
        return qs.filter(project_id=project.id)


class RevisionUserTextInline(admin.StackedInline):
    model = RevisionUserText
    # max_num=maximun number of editable objects inline, extra=additional object that doesn't exceed max_num
    extra = 0
    readonly_fields = ('created_at','updated_at',)

# customize Comment model in admin panel
class UserTextAdmin(admin.ModelAdmin):
    # add revisions of comment as inline editable object
    inlines = [
        RevisionUserTextInline,
    ]

    list_display = ('urn', 'content_type', 'privacy', 'project')

    # customized query set to overwrite filter with hostname related permissions
    def get_queryset(self, request):
        qs = super(UserTextAdmin, self).get_queryset(request)
        project = get_project_from_request(request)
        return qs.filter(project_id=project.id)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "selected_revision":
            # NOTE: (charles) This is a little hacky. Is it just me,
            # or is it weird that Django Admin doesn't make it easier
            # to get the REST params from the URL?
            object_id = request.resolver_match.kwargs.get('object_id')

            if object_id is not None:
                kwargs["queryset"] = RevisionUserText.objects.filter(user_text_id=object_id)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

# add customized admin models to dashboard
dashboard.register(CommentarySettings, CommentarySettingsAdmin)
dashboard.register(UserProfile, UserProfileAdmin)
dashboard.register(Comment, CommentAdmin)
dashboard.register(Tag, TagAdmin)
dashboard.register(UserText, UserTextAdmin)
