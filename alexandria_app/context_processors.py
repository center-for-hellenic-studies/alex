import json
from django.core import serializers
from .lib.get_archive_hostname import get_archive_hostname
from .lib.commentary_texts import COMMENTARY_TEXTS
from .lib.commentary_commenters import COMMENTARY_COMMENTERS
from .models import Project
# from .models import Commenter


def tenant_by_hostname(request):
    hostname = get_archive_hostname(request)
    project = None
    project_texts = []
    project_commenters = []

    # is tenant index route
    try:
        project = Project.objects.get(hostname=hostname)
    except Project.DoesNotExist:
        project = None

    if project:
        # get commentary texts
        for commentary in COMMENTARY_TEXTS:
            if commentary['hostname'] == project.hostname:
                project_texts = commentary['texts']

        # get commenters for commentary
        for commentary in COMMENTARY_COMMENTERS:
            if commentary['hostname'] == project.hostname:
                project_commenters = commentary['commenters']

    if project:
        return {
            'project': project,
            'project_texts': project_texts,
            'project_commenters': project_commenters,
            'project_json': {
                'id': project.id,
                'title': project.title,
                'hostname': project.hostname,
            }
        }
    else:
        return {}
