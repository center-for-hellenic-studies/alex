import graphene
from graphene_django.types import DjangoObjectType

from alexandria_app.models import Comment
from .revision_comment import RevisionCommentType

class CommentType(DjangoObjectType):
    class Meta:
        model = Comment

    latest_revision = graphene.Field(RevisionCommentType)

    def resolve_latest_revision(parent, info, **kwargs):
        # the first() revision is the latest entry
        return parent.revisions.first()