import graphene
from graphene_django.types import DjangoObjectType
from django.core.paginator import Paginator

from alexandria_app.models import Project, Comment
from .comment import CommentType

from ...lib.parse_urn import parse_urn, get_passages_from_urn, is_passage_in_range

class ProjectType(DjangoObjectType):
    comments = graphene.List(CommentType, 
        urn_search=graphene.String(),
        commenter_name_search=graphene.String(),
        search_term=graphene.String(),
        page_size=graphene.Int(default_value=10), 
        page_number=graphene.Int(default_value=0)
    )

    class Meta:
        model = Project

    # TODO: store passage from and to in model for easier and more performant filtering
    def resolve_comments(parent, info, **kwargs):
        # root query set with project filter
        comment_list_qs = Comment.objects.filter(project_id=parent.id)

        # search by keyword/tag
        search_term = kwargs.get('search_term')
        if search_term:
            comment_list_qs = comment_list_qs.filter(revisions__text__contains=search_term) | comment_list_qs.filter(tags__name__contains=search_term) 

        # search by name of commenter
        commenter_name_search = kwargs.get('commenter_name_search')
        if commenter_name_search:
            comment_list_qs = comment_list_qs.filter(commenters__username__contains=commenter_name_search) | comment_list_qs.filter(commenters__first_name__contains=commenter_name_search) | comment_list_qs.filter(commenters__last_name__contains=commenter_name_search) 

        # de-duplication
        comment_list_qs = comment_list_qs.distinct()

        # merged commentsOn: filter with urn
        # TODO: pagination won't work unless refactored into returning a queryset with filters, 
        # probably would require storing passage from and to in model
        urn_search = kwargs.get('urn_search')
        if urn_search:
            text_group = parse_urn(urn_search)
            query_passage_range = get_passages_from_urn(urn_search)
            result_comments = []
            #print(urn, text_group, get_passages_from_urn(urn))
            text_group_comments = comment_list_qs.filter(urn__contains=text_group)
            for comment in text_group_comments:
                if is_passage_in_range(get_passages_from_urn(comment.urn), query_passage_range):
                    result_comments.append(comment)
            return result_comments

        # pagination
        paginator = Paginator(comment_list_qs, kwargs.get('page_size'))
        page_obj = paginator.get_page(kwargs.get('page_number'))
        return page_obj
