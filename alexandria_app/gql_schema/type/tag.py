from graphene_django.types import DjangoObjectType

from alexandria_app.models import Tag

class TagType(DjangoObjectType):
    class Meta:
        model = Tag