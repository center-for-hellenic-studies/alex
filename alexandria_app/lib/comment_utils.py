from .parse_urn import parse_urn, get_passages_from_urn, is_passage_in_range, range_contains_passage
from ..models import Comment, Project
from .textserver import Textserver

from pprint import pprint


def get_comments_from_urn(urn, queryset=Comment.objects.all()):
    # filter by textgroup
    text_group = parse_urn(urn)
    query_passage_range = get_passages_from_urn(urn)
    result_comments = []
    text_group_comments = queryset.filter(urn__contains=text_group)
    # filter with passage range
    for comment in text_group_comments:
        # print('...p,tp:', get_passages_from_urn(
        #     comment.urn), query_passage_range)
        comment_urn = comment.urn
        comment_passage_range = get_passages_from_urn(comment_urn)
        comment_from = comment_passage_range.get('from')
        if range_contains_passage(query_passage_range, comment_from):
            # is_passage_in_range was returning incorrect comments when working
            # with a single textNode
        # if is_passage_in_range(get_passages_from_urn(comment.urn), query_passage_range):
            # print('...p,tp:', get_passages_from_urn(
            #     comment.urn), query_passage_range)
            result_comments.append(comment)
            #print('...r', result_comments)
    return result_comments

def get_counts_by_urn(urn, queryset=Comment.objects):
    text_group = parse_urn(urn, format='text_group')
    query_passage_range = get_passages_from_urn(urn)
    text_group_comments = queryset.filter(urn__contains=text_group)
    results = {}

    for comment in text_group_comments:
        comment_urn = comment.urn
        comment_passage_range = get_passages_from_urn(comment_urn)
        comment_from = comment_passage_range.get('from')

        if range_contains_passage(query_passage_range, comment_from):
            k = '.'.join(map(str, comment_from))
            if results.get(k):
                results[k] += 1
            else:
                results[k] = 1

    return results

def get_counts_for_work_by_urn(urn, queryset=Comment.objects):
    text_group = parse_urn(urn, format='text_group')
    text_group_comments = queryset.filter(urn__contains=text_group)
    results = {}

    for comment in text_group_comments:
        comment_urn = comment.urn
        comment_passage_range = get_passages_from_urn(comment_urn)
        comment_from = comment_passage_range.get('from')

        k = '.'.join(map(str, comment_from))
        if results.get(k):
            results[k] += 1
        else:
            results[k] = 1

    return results


def get_commentaries_from_urn(urn):
    return Project.objects.filter(comment__urn__contains=urn).distinct()

def get_unique_textgroups_from_urns(urns):
    unique_textgroup_urns = []
    for urn in urns:
        textgroup_urn = parse_urn(urn, format='text_group')
        if textgroup_urn and textgroup_urn not in unique_textgroup_urns:
            unique_textgroup_urns.append(textgroup_urn)
    return unique_textgroup_urns

def get_texts_from_comments_urns(comments_urns):
    unique_textgroup_urns = get_unique_textgroups_from_urns(comments_urns)
    textserver = Textserver()
    print(unique_textgroup_urns)
    works = textserver.fetch_works_by_urn_list(unique_textgroup_urns)
    return works