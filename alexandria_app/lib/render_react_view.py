"""
Render a react view. Based on https://github.com/dhmit/rereading/blob/master/backend/apps/common.py
"""
from django.shortcuts import render

from .get_archive_hostname import get_archive_hostname
from ..models import Project


def render_react_view(request, template="react_view.twig", context=None):
    """
    A view function to render views that are entirely managed
    in the frontend by a single React component. This lets us use
    Django url routing with React components.

    :param request: Django request object to pass through
    :param template: name of template to render
    :param context: Django context

    :return:
    """
    user_is_admin = 0
    user_is_editor = 0
    user_is_commenter = 0
    user_is_logged_in = 0

    if request.user.is_authenticated:
        user_is_logged_in = 1
        hostname = get_archive_hostname(request)
        user = request.user
        try:
            user_is_admin = len(Project.objects.filter(
                admins=user, hostname=hostname))
            user_is_editor = len(Project.objects.filter(
                editors=user, hostname=hostname))
            user_is_commenter = len(Project.objects.filter(
                commenters=user, hostname=hostname))
        except:
            pass
    
    context['USER_IS_ADMIN'] = user_is_admin
    context['USER_IS_EDITOR'] = user_is_editor
    context['USER_IS_COMMENTER'] = user_is_commenter
    context['USER_IS_LOGGED_IN'] = user_is_logged_in

    return render(request, template, context)
