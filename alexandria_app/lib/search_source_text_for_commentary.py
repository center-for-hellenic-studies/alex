import requests
from .project_content_fetcher import get_relevant_works

SEARCH_QUERY = """
	query textNodeSearchQuery(
		$after: Int
		$before: Int
		$first: Int
		$last: Int
	) {{
		results: textNodeSearch(
			after: $after
			before: $before
			first: $first
			last: $last
			textSearch: "{}"
			workId: {}
		) {{
			pageInfo {{
				count
				hasNextPage
				hasPreviousPage
				pages
			}}
			textNodes {{
				id
				location
				text
				urn
			}}
		}}
	}}
"""


def search_source_text_for_commentary(project, search_term):
    source_text_results = []
    translation_results = []

    (relevant_works, _) = get_relevant_works(project)

    for work in relevant_works['works']:
        for work_edition in work['works']:
            query = SEARCH_QUERY.format(search_term, work_edition['id'])
            request = requests.post('http://text.chs.harvard.edu/graphql', json={'query': query})
            results = request.json()
            for text_node in results['data']['results']['textNodes']:
                text_node['work'] = work_edition

                if work_edition['work_type'] == "translation":
                    translation_results.append(text_node)
                else:
                    source_text_results.append(text_node)


    return source_text_results, translation_results
