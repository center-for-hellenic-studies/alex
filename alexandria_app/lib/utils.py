from urllib.parse import urlparse

from django.urls import reverse
from django.utils.http import urlencode

from .textserver import Textserver
from .parse_urn import parse_urn

COMMENT_CITATION_URN_PREFIX = 'urn:cts'
ALEX_NAMESPACE = 'ALEX'


def get_url_with_modified_subdomain(hostname, current_url):
    """modify only the first subdomain of the url passed in

    Arguments:
        hostname {[type]} -- [description]
        current_url {[type]} -- [description]
    """
    current_url_parsed = urlparse(current_url)
    hostname_array = current_url_parsed.netloc.split('.')
    hostname_array[0] = hostname
    new_url = current_url_parsed._replace(netloc='.'.join(hostname_array))
    return new_url.geturl()


def generate_comment_citation_urn(comment):
    """generate comment citation URN that we defined, in the format of
    <PREFIX>:<ALEX_NAMESPACE>:<TYPE.HOSTNAME>:<WORK_TITLE>:<CITATION_PASSAGES>:<ALEX_COMMENT_ID>
    eg. urn:cts:ALEX:Commentaries.homer:Iliad.1.1-1.611:1234

    Arguments:
        comment {[type]} -- [description]

    Returns:
        [type] -- [description]
    """
    # generate type/hostname
    type_and_hostname = f'Commentaries.{comment.project.hostname}'
    # generate work title with textserver data
    work_urn = parse_urn(comment.urn)
    textserver = Textserver()
    works = textserver.fetch_works_by_urn_list([work_urn])
    first_work = works['workSearch']['works'][0]
    work_title = first_work['english_title']
    # get passages from urn
    passage_from_to = parse_urn(comment.urn, 'passages')
    comment_citation_urn_generated = f'{COMMENT_CITATION_URN_PREFIX}:{ALEX_NAMESPACE}:{type_and_hostname}:{work_title}:{passage_from_to}:{comment.id}'
    return comment_citation_urn_generated

def pick_a_text(texts):
    """pick one text from texts that is Edition type and has full_urn

    Arguments:
        texts {[type]} -- [description]
    """
    if len(texts) < 1:
        return None
    text_chosen = texts[-1]
    for text in texts:
        if not text['language']:
            continue
        if text['work_type'] == 'edition':
            text_chosen = text
            if text['full_urn']:
                text_chosen = text
                return text_chosen
    return text_chosen


def get_params(request):
    params_named = {}
    param_names = ['Collection', 'Language', 'Work Type', 'Structure', 'Author', 'search', 'page']
    for param_name in param_names:
        param_value = request.GET.get(param_name)
        if param_value:
            params_named[param_name] = param_value
    return params_named


def reverse_querystring(view, urlconf=None, args=None, kwargs=None, current_app=None, query_kwargs=None):
    '''Custom reverse to handle query strings.
    Usage:
        reverse('app.views.my_view', kwargs={'pk': 123}, query_kwargs={'search', 'Bob'})
    '''
    base_url = reverse(view, urlconf=urlconf, args=args, kwargs=kwargs, current_app=current_app)
    if query_kwargs:
        return '{}?{}'.format(base_url, urlencode(query_kwargs))
    return base_url
