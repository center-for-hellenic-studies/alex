import React from 'react';
import { shallow } from 'enzyme';

// component
import FileUpload from './FileUpload';

describe('FileUpload', () => {
	it('renders correctly', () => {

		const wrapper = shallow(
			<FileUpload
				file={{ name: 'test' }}
			/>
		);
		expect(wrapper).toBeDefined();
	});
});
