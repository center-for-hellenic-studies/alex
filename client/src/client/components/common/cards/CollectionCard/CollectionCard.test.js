import React from 'react';
import { shallow } from 'enzyme';

// component
import CollectionCard from './CollectionCard';

describe('CollectionCard', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<CollectionCard to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
