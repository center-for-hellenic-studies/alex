import React from 'react';
import PropTypes from 'prop-types';

import CollectionCard from '../../cards/CollectionCard';

import { Checkbox, Button, Typography } from '@material-ui/core';


const HorizontalList = (props) => {

	const _classes = [];
	_classes.push('horizontalList');

	if (props.loading) {
		_classes.push('-loading');
	}

	if (props.fullWidth) {
		_classes.push('-fullWidth');
	}

	if (props.className) {
		_classes.push(props.className);
	}

	if (!props.items.length && !props.loading) {
		return null;
	}

	return (
		<div className={_classes.join(' ')}>
			{!props.loading && props.title ?

				<Typography variant="overline" className="horizontalListTitle">
					{props.items.length} {props.title}
				</Typography>
				: ''}
			<div className="horizontalListButton">
				<Button href={props.link}>
					View All
				</Button>
			</div>
			<div className="horizontalListItems">
				{props.items.map(item => (
					<CollectionCard
						loading={props.loading}
						compact={props.compact}
						type={props.type}
						key={item._id}
						className="horizontalListItem"
						{...item}
					/>
				))}
			</div>
		</div>
	);
};

HorizontalList.propTypes = {
	horizontalListItems: PropTypes.array,
	loading: PropTypes.bool,
	fullWidth: PropTypes.bool,
	className: PropTypes.string,
	compact: PropTypes.bool,
	type: PropTypes.string,
};

HorizontalList.defaultProps = {
	horizontalListItems: [],
};

export default HorizontalList;
