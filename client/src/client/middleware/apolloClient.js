import Cookies from 'universal-cookie';
import {
	ApolloClient,
	ApolloLink,
	HttpLink,
	InMemoryCache,
	from,
} from '@apollo/client';
import { RestLink } from 'apollo-link-rest';
import { onError } from '@apollo/client/link/error';
import { setContext } from '@apollo/client/link/context';

const cookies = new Cookies();
const token = cookies.get('token') || null;
const csrfToken = cookies.get('csrftoken');
const GRAPHQL_URL = process.env.REACT_APP_GRAPHQL_URL || 'http://localhost:3003/graphql';
const APP_SERVER = process.env.REACT_APP_SERVER;

const httpLink = new HttpLink({
	uri: GRAPHQL_URL,
	credentials: 'same-origin',
	fetch,
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
	if (graphQLErrors)
		graphQLErrors.map(({ message, locations, path }) =>
			console.log(
				`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
			),
		);

	if (networkError) console.log(`[Network error]: ${networkError}`);
});

const authLink = setContext((_, { headers }) => {
	return {
		headers: {
			...headers,
			// authorization: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YUhWSE82MSIsInVzZXJOYW1lIjoidGVzdEBhcmNoaW1lZGVzLmRpZ2l0YWwiLCJleHAiOjE2MDIzMDk5NTYsImlhdCI6MTU3MDc3Mzk1Nn0.UJ0dZt1ITCSqwM3zIjzH_LzJVcxhZsI5G6KM7Q7rxv0",
			'X-CSRFToken': csrfToken,
		}
	}
});

function customFetch(url, options) {
	const headers = {
		Accept: 'application/json',
		'Content-Type': 'application/json',
		'X-CSRFToken': csrfToken
	};

  return fetch(url, {
  	...options,
  	headers,
  });
}

const restLink = new RestLink({
	credentials: 'same-origin',
	customFetch,
	headers: {
		'X-CSRFToken': csrfToken,
	},
	headersToOverride: ['Accept', 'Content-Type', 'X-CSRFToken'],
	// fall back to document.location.origin
	uri: `${APP_SERVER || document.location.origin}/api/`
});

const client = new ApolloClient({
	cache: new InMemoryCache().restore(window.__APOLLO_STATE__),
	link: ApolloLink.from([errorLink, authLink, restLink, httpLink]),
	uri: GRAPHQL_URL,
});

export default client;
