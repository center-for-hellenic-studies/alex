import React from 'react';
import { shallow } from 'enzyme';

// component
import Card from './Card';

describe('Card', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Card to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
