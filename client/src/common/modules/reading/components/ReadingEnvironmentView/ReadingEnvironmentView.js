/**
 * @prettier
 */

import { hot } from 'react-hot-loader/root';

import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import App from '#common/reading/components/App';
import Root from '#common/containers/Root';

const ReadingEnvironmentView = ({ match }) => (
  <Root>
    <BrowserRouter>
      <Routes>
        <Route path={`${match.url}/:urn`} element={<App />} />
      </Routes>
    </BrowserRouter>
  </Root>
);

ReadingEnvironmentView.propTypes = {
  urn: PropTypes.string.isRequired,
};

const ReadingEnv = <Route path="/read" element={ReadingEnvironmentView} />;

export default hot(ReadingEnv);
