import Cookies from 'universal-cookie';
import fetch, { Headers } from 'cross-fetch';
import { useMemo } from 'react';
import {
	ApolloClient,
	ApolloLink,
	HttpLink,
	InMemoryCache,
} from '@apollo/client';
import { RestLink } from 'apollo-link-rest';
import { onError } from '@apollo/client/link/error';
import { setContext } from '@apollo/client/link/context';

// polyfill Headers for RestLink
// see https://github.com/Rsullivan00/apollo-link-json-api/issues/27
global.Headers = Headers;

const GRAPHQL_URL = process.env.REACT_APP_GRAPHQL_URL || 'http://text.chs.harvard.edu/graphql';
const APP_SERVER = process.env.REACT_APP_SERVER || 'http://alexandria.staging.archimedes.digital';

const errorLink = onError(({ graphQLErrors, networkError }) => {
	if (graphQLErrors)
		graphQLErrors.map(({ message, locations, path }) =>
			console.log(
				`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
			),
		);

	if (networkError) console.log(`[Network error]: ${networkError}`);
});


function createHttpLink(req) {
	return new HttpLink({
		fetch,
		headers: {
			cookie: req.header('Cookie'),
		},
		uri: GRAPHQL_URL,
	});
}

function customFetch(url, options) {
  return fetch(url, options);
}

export function createApolloClient(req) {
	const httpLink = createHttpLink(req);
	const restLink = new RestLink({
		customFetch,
		headers: {
			Host: req.hostname,
		},
		uri: `${APP_SERVER}/api/`
	});

	return new ApolloClient({
		cache: new InMemoryCache(),
		link: ApolloLink.from([errorLink, restLink, httpLink]),
		ssrMode: true,
	});
}
