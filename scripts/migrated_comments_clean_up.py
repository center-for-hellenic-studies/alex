# python manage.py runscript migrated_comments_clean_up
# 0. set private for <p>-</p>
# 1. set private, set commenter as Nagy for commenters []

from alexandria_app.models import Comment, CustomUser

def set_private_empty_body(matcher):
    blank_comments = Comment.objects.filter(revisions__text__iexact=matcher)
    print(f'[info] found {len(blank_comments)} comments with {matcher}')
    for comment in blank_comments:
        comment.privacy = 'PRIVATE'
        comment.save()
        print(f'[info] comment id {comment.id} set PRIVATE with content {comment.revisions.all()[0].text}')

def nagy_owns_commenter_less_comments():
    nagy = CustomUser.objects.get(full_name='Gregory Nagy')
    print(f'[info] User to be associated: {nagy}')
    comments_to_own = Comment.objects.filter(commenters=None)
    print(f'[info] found {len(comments_to_own)} comments without commenters')
    for comment in comments_to_own:
        comment.privacy = 'PRIVATE'
        comment.commenters.add(nagy)
        comment.save()
        print(f'[info] comment id {comment.id} set PRIVATE with commenter {comment.commenters.all()}')

def run(*args):
    set_private_empty_body('<p>-</p>')
    set_private_empty_body('<p> </p>')
    nagy_owns_commenter_less_comments()