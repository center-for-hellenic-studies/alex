# sync comments from ahcip
# read from ahcip, find+update/create comment in alex
from pprint import pprint

from alexandria_app.models import Comment, Project, RevisionComment, CustomUser, Tag

from .lib.ahcip import Ahcip, get_work_title_and_passages_from_legacy_comment_citation_urn


class AHCIP_Migration:
    WORK_TITLE_TO_URN = {
        'Odyssey': 'urn:cts:greekLit:tlg0012.tlg002',
        'Iliad': 'urn:cts:greekLit:tlg0012.tlg001',
        'HomericHymns': 'urn:cts:greekLit:tlg0013.',
        'Isthmian': 'urn:cts:greekLit:tlg0033.tlg004',
        'Olympian': 'urn:cts:greekLit:tlg0033.tlg001',
        'Pythian': 'urn:cts:greekLit:tlg0033.tlg002',
        'Nemean': 'urn:cts:greekLit:tlg0033.tlg003',
        'Descriptionof Greece': "urn:cts:greekLit:tlg0525.tlg001",
    }

    def __init__(self, batch, hostname):
        self.hostname = hostname
        self.batch = batch

    # read comments from ahcip
    def extract(self):
        limit = 100
        skip = limit*self.batch
        print('[INFO] Processing batch#', self.batch)
        ahcip = Ahcip()
        ahcip_comments = ahcip.fetch_comments(limit, skip, self.hostname)
        return ahcip_comments

    # create alex-django comments
    def transform_and_load(self, ahcip_comment):
        # get comment.project
        project = Project.objects.get(hostname=self.hostname)

        # generate comment.urn
        work_title, work_passages = get_work_title_and_passages_from_legacy_comment_citation_urn(ahcip_comment)
        
        if work_title == 'HomericHymns':
            comment_work_urn = f"{self.WORK_TITLE_TO_URN[work_title]}{ahcip_comment['lemmaCitation']['work']}"
        else:
            comment_work_urn = self.WORK_TITLE_TO_URN[work_title]
        comment_urn = f'{comment_work_urn}:{work_passages}'
        print('[INFO] comment from AHCIP - ', work_title, work_passages, comment_work_urn, comment_urn)

        # get comment_citation_urn from legacy comment.v2.urn
        comment_citation_urn = ahcip_comment['urn']['v2']

        # parse privacy
        comment_privacy = 'PUBLIC' if ahcip_comment['status'] == 'publish' else 'PRIVATE'

        # find existing comment with matching comment_citation_urn or build comment instance
        comment = None
        try:
            comment = Comment.objects.get(comment_citation_urn=comment_citation_urn)
            comment.urn = comment_urn
            comment.save()
        except Comment.DoesNotExist:
            comment = Comment(
                project=project,
                urn=comment_urn,
                comment_citation_urn=comment_citation_urn,
                privacy=comment_privacy,
            )
            comment.save()

        # remove existing revisions
        comment.revisions.all().delete()
        # parse revisions from ahcip
        for revision in ahcip_comment['revisions']:
            comment_revision = RevisionComment(
                comment=comment,
                title=revision['title'],
                text=revision['text'],
                text_raw=revision['textRaw'],
                created_at=revision['created'],
            )
            comment_revision.save()

        # clear commenters property
        comment.commenters.clear()
        # parse commenters
        for commenter in ahcip_comment['commenters']:
            # this will raise exception if user isn't found, which is expected
            try:
                comment_commenter = CustomUser.objects.get(full_name=commenter['name'])
            except:
                print('[ERROR] Cannot find Commenter - ', commenter['name'])
                print(Comment.objects.filter(project__hostname=self.hostname).count())
                exit(1)
            comment.commenters.add(comment_commenter)

        # clear commenters property
        comment.tags.clear()
        # parse keywords/tags
        for keyword in ahcip_comment['keywords']:
            # find or create a Tag instance with the keyword as name
            tag, created = Tag.objects.get_or_create(
                project=project,
                name=keyword['title'],
            )
            comment.tags.add(tag or created)

        print('[INFO]comment created/updated - ', comment.project, comment.urn, comment.comment_citation_urn, \
            comment.privacy, comment.commenters.all(), comment.tags.all())

    def start(self):
        while(1):
            # Extract
            ahcip_comments = self.extract()

            # Transform and Load
            for ahcip_comment in ahcip_comments['comments']:
                comment_instance = self.transform_and_load(ahcip_comment)

            self.batch+=1
            if self.batch >= 1:
                #break
                pass

            if len(ahcip_comments['comments']) == 0:
                break


def run(*args):
    # eg. python manage.py runscript sync_from_ahcip_comments --script-args 0 pindar
    print(args)
    batch = int(args[0])
    hostname = str(args[1])

    migration = AHCIP_Migration(batch, hostname)
    migration.start()



